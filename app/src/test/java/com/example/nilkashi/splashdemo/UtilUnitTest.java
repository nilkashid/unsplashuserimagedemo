package com.example.nilkashi.splashdemo;


import com.example.nilkashi.splashdemo.utils.Util;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class UtilUnitTest
{
    @Test
    public void getRandomImageUrlTest()
    {
        // given

        // when
        String randomImageAPIUrl = Util.getRandomImageUrl();

        //then
        assertNotNull(randomImageAPIUrl);

    }

    @Test
    public void getCommonHeaderParamsTest()
    {
        // given

        // when
        Map<String, String> headerParams = Util.getCommonHeaderParams();

        //then
        assertNotNull(headerParams);

    }
}
