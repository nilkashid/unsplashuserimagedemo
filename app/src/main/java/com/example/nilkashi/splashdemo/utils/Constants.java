package com.example.nilkashi.splashdemo.utils;


public final class Constants
{
    public final class  NetworkConstants {
        public static final String BASE_URL = "https://api.unsplash.com/";
        public static final String RANDOM_IMAGES_URL = "photos/random";
        public static final int IMAGE_PAGE_COUNT = 180;
        public static final String IMAGE_PAGE_COUNT_KEY = "count";
        public static final String HEADER_AUTHORIZATION_KEY = "Authorization";
    }
}
