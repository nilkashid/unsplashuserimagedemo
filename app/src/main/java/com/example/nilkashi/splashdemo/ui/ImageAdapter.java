package com.example.nilkashi.splashdemo.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.nilkashi.splashdemo.R;
import com.example.nilkashi.splashdemo.data.bo.ImageBo;

import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

    private List<ImageBo> imageBos;
    private final OnItemClickListener listener;


    public void add(int position, ImageBo item) {
        imageBos.add(position, item);
        notifyItemInserted(position);
    }

    public ImageAdapter(List<ImageBo> myDataset, OnItemClickListener listener) {
        imageBos = myDataset;
        this.listener = listener;
    }

    @Override
    public ImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ImageBo imageBo = imageBos.get(position);
        Glide.with(holder.imageView.getContext()).load(imageBo.getSmallPath()).into(holder.imageView);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                listener.onItemClick(imageBo);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageBos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public ViewHolder(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.image);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(ImageBo imageBo);
    }
}
