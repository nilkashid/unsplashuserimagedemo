package com.example.nilkashi.splashdemo.ui;

import com.example.nilkashi.splashdemo.data.bo.ImageBo;

import java.util.List;

public interface UnSplashGridView
{
    void showLoader();
    void hideLoader();
    void displayMessage(String message);
    void loadImageList(List<ImageBo> images);
}
