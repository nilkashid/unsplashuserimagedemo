package com.example.nilkashi.splashdemo.repository.network;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.nilkashi.splashdemo.interactor.UnSplashImageDataInteractor;
import com.example.nilkashi.splashdemo.utils.Util;

import java.util.Map;

public class UnSplashNetworkRepoImpl implements UnSplashNetworkRepo {

    @Override
    public void getImageDataFromServer(Context context, final UnSplashImageDataInteractor.CompletionHandler handler) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = Util.getRandomImageUrl();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        handler.successResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handler.errorResponse(400, error.getMessage());
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return Util.getCommonHeaderParams();
            }
        };
        queue.add(stringRequest);
    }
}
