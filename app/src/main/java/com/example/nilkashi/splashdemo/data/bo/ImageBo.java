package com.example.nilkashi.splashdemo.data.bo;


public class ImageBo
{
    String regularPath;
    String smallPath;
    String thumPath;

    public String getRegularPath() {
        return regularPath;
    }

    public void setRegularPath(String regularPath) {
        this.regularPath = regularPath;
    }

    public String getSmallPath() {
        return smallPath;
    }

    public void setSmallPath(String smallPath) {
        this.smallPath = smallPath;
    }

    public String getThumPath() {
        return thumPath;
    }

    public void setThumPath(String thumPath) {
        this.thumPath = thumPath;
    }
}
