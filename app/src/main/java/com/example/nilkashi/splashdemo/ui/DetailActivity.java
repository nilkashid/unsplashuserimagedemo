package com.example.nilkashi.splashdemo.ui;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.nilkashi.splashdemo.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_detail)
public class DetailActivity extends AppCompatActivity {

    @ViewById
    ImageView imageView;
    private ProgressDialog loader;
    private String imagePath;
    public static final String INTENT_EXTRA_IMAGE_PATH = "imageExtra";
    @AfterViews
    protected void init() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        initBundleData();
        initUI();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
           finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void initUI(){
        loader = new ProgressDialog(this);
        loader.setTitle(getString(R.string.loading));
        loader.setCancelable(true);

        if(!imagePath.isEmpty()){
            showLoader();
            Glide.with(imageView.getContext())
                    .load(imagePath)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            hideLoader();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            hideLoader();
                            return false;
                        }
                    })
                    .into(imageView);
        }else {
            displayMessage(getResources().getString(R.string.image_error));
        }
    }
    private void initBundleData(){
        if(getIntent() != null && getIntent().hasExtra(INTENT_EXTRA_IMAGE_PATH)){
            imagePath = getIntent().getStringExtra(INTENT_EXTRA_IMAGE_PATH);
        }
    }

    public void showLoader() {
        loader.show();
    }

    public void hideLoader() {
        loader.hide();
    }
    public void displayMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
