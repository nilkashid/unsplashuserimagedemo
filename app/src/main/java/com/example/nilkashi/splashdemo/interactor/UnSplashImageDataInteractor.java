package com.example.nilkashi.splashdemo.interactor;

import android.content.Context;

public interface UnSplashImageDataInteractor {

    interface CompletionHandler{
        void successResponse(String data);
        void errorResponse(int errorCode, String message);
    }

    void getImageDataFromServer(Context context);
}
