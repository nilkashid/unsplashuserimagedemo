package com.example.nilkashi.splashdemo.helper;

import com.example.nilkashi.splashdemo.data.bo.ImageBo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BeanHelper
{
    public static List<ImageBo> getImages(String dataObject) throws JSONException{


        JSONArray userData = new JSONArray(dataObject);
        List<ImageBo> images = new ArrayList<>();

        for(int i = 0; i<userData.length(); i++)
        {
            JSONObject user = userData.getJSONObject(i);

            if(user != null && user.has("urls")){
                JSONObject imageData = user.getJSONObject("urls");
                images.add(getImageData(imageData));
            }
        }
        return images;
    }

    private static ImageBo getImageData(JSONObject imageData) throws JSONException{
        ImageBo imageBo = new ImageBo();
        if(imageData != null){
            imageBo.setRegularPath(imageData.has("regular") ? imageData.getString("regular") : "");
            imageBo.setSmallPath(imageData.has("small") ? imageData.getString("small") : "");
            imageBo.setThumPath(imageData.has("thumb") ? imageData.getString("thumb") : "");
        }
        return imageBo;
    }
}
