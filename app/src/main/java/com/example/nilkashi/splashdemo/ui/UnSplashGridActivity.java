package com.example.nilkashi.splashdemo.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.widget.Toast;

import com.example.nilkashi.splashdemo.R;
import com.example.nilkashi.splashdemo.data.bo.ImageBo;
import com.example.nilkashi.splashdemo.presenter.UnSplashImageDataPresentor;
import com.example.nilkashi.splashdemo.presenter.UnSplashImageDataPresentorImpl;
import com.example.nilkashi.splashdemo.utils.Util;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EActivity(R.layout.activity_main)
public class UnSplashGridActivity extends AppCompatActivity implements UnSplashGridView, ImageAdapter.OnItemClickListener {

    @ViewById
    RecyclerView recycleView;

    private StaggeredGridLayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private ProgressDialog loader;
    private UnSplashImageDataPresentor unSplashImageDataPresentor;


    @AfterViews
    protected void init() {
        unSplashImageDataPresentor = new UnSplashImageDataPresentorImpl(this);
        initUI();
        loadImageData();
    }


    private void loadImageData() {
        if(Util.isNetworkAvailable(this)) {
            unSplashImageDataPresentor.getImageData(this);
        }else{
            displayMessage(getResources().getString(R.string.internet_connection));
        }
    }

    private void initUI(){
        recycleView.setHasFixedSize(true);
        layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        recycleView.setLayoutManager(layoutManager);

        loader = new ProgressDialog(this);
        loader.setTitle(getString(R.string.loading));
        loader.setCancelable(false);
    }

    @Override
    public void showLoader() {
        loader.show();
    }

    @Override
    public void hideLoader() {
        loader.hide();
    }

    @Override
    public void displayMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadImageList(List<ImageBo> images) {
        adapter = new ImageAdapter(images, this);
        recycleView.setAdapter(adapter);
    }


    @Override
    public void onItemClick(ImageBo imageBo) {
        startDetailActivity(imageBo.getRegularPath());
    }

    public void startDetailActivity(String imagePath){
        Intent intent = new Intent(this, DetailActivity_.class);
        intent.putExtra(DetailActivity_.INTENT_EXTRA_IMAGE_PATH, imagePath);
        startActivity(intent);
    }
}