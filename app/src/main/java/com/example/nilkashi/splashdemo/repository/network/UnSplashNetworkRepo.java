package com.example.nilkashi.splashdemo.repository.network;

import android.content.Context;

import com.example.nilkashi.splashdemo.interactor.UnSplashImageDataInteractor;

public interface UnSplashNetworkRepo
{
    void getImageDataFromServer(Context context, UnSplashImageDataInteractor.CompletionHandler handler);
}
