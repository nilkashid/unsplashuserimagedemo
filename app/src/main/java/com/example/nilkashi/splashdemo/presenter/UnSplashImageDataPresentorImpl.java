package com.example.nilkashi.splashdemo.presenter;

import android.content.Context;

import com.example.nilkashi.splashdemo.data.bo.ImageBo;
import com.example.nilkashi.splashdemo.interactor.UnSplashImageDataInteractor;
import com.example.nilkashi.splashdemo.interactor.UnSplashImageInteractorImpl;
import com.example.nilkashi.splashdemo.ui.UnSplashGridView;

import java.util.List;

public class UnSplashImageDataPresentorImpl implements UnSplashImageDataPresentor {

    private UnSplashGridView unSplashGridView;
    private UnSplashImageDataInteractor unSplashImageDataInteractor;

    public UnSplashImageDataPresentorImpl(UnSplashGridView unSplashGridView)
    {
        this.unSplashGridView = unSplashGridView;
        unSplashImageDataInteractor = new UnSplashImageInteractorImpl(this);
    }
    @Override
    public void getImageDataSuccess(List<ImageBo> imageBos) {
        unSplashGridView.loadImageList(imageBos);
        unSplashGridView.hideLoader();
    }

    @Override
    public void getImageDataFailed(String errorMessage) {
        unSplashGridView.displayMessage(errorMessage);
        unSplashGridView.hideLoader();
    }

    @Override
    public void getImageData(Context context) {
        unSplashGridView.showLoader();
        unSplashImageDataInteractor.getImageDataFromServer(context);
    }
}
