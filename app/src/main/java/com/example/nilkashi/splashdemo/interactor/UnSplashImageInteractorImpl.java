package com.example.nilkashi.splashdemo.interactor;

import android.content.Context;

import com.example.nilkashi.splashdemo.data.bo.ImageBo;
import com.example.nilkashi.splashdemo.helper.BeanHelper;
import com.example.nilkashi.splashdemo.presenter.UnSplashImageDataPresentor;
import com.example.nilkashi.splashdemo.repository.network.UnSplashNetworkRepo;
import com.example.nilkashi.splashdemo.repository.network.UnSplashNetworkRepoImpl;

import org.json.JSONException;

import java.util.List;

public class UnSplashImageInteractorImpl implements UnSplashImageDataInteractor
{
    private UnSplashImageDataPresentor unSplashImageDataPresentor;
    private UnSplashNetworkRepo unSplashNetworkRepo;

    public UnSplashImageInteractorImpl(UnSplashImageDataPresentor unSplashImageDataPresentor){
        this.unSplashImageDataPresentor = unSplashImageDataPresentor;
        unSplashNetworkRepo = new UnSplashNetworkRepoImpl();
    }

    @Override
    public void getImageDataFromServer(Context context) {

        unSplashNetworkRepo.getImageDataFromServer(context, new CompletionHandler() {
            @Override
            public void successResponse(String data) {
                    try {
                        List<ImageBo> images = BeanHelper.getImages(data);
                        unSplashImageDataPresentor.getImageDataSuccess(images);
                    }catch (JSONException ex) {
                        unSplashImageDataPresentor.getImageDataFailed(ex.getMessage());
                    }
            }

            @Override
            public void errorResponse(int errorCode, String message) {
                unSplashImageDataPresentor.getImageDataFailed(message);
            }
        });
    }
}
