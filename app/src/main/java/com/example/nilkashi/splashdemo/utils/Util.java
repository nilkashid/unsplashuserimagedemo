package com.example.nilkashi.splashdemo.utils;

import android.content.Context;
import android.net.ConnectivityManager;

import java.util.HashMap;
import java.util.Map;

public class Util
{
    public static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static String getRandomImageUrl(){
        return Constants.NetworkConstants.BASE_URL+
                Constants.NetworkConstants.RANDOM_IMAGES_URL+"?"+
                Constants.NetworkConstants.IMAGE_PAGE_COUNT_KEY+"="+
                Constants.NetworkConstants.IMAGE_PAGE_COUNT;
    }

    public static Map<String, String> getCommonHeaderParams()
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constants.NetworkConstants.HEADER_AUTHORIZATION_KEY, "Client-ID b94e8f3d44736ad3d5e227d02caced65a1f3f26cd6c9e3b1aaff900aa7944fd1");

        return params;
    }
}
