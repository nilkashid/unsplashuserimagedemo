package com.example.nilkashi.splashdemo.presenter;

import android.content.Context;

import com.example.nilkashi.splashdemo.data.bo.ImageBo;

import java.util.List;

public interface UnSplashImageDataPresentor
{
    void getImageDataSuccess(List<ImageBo> imageBos);
    void getImageDataFailed(String errorMessage);
    void getImageData(Context context);
}
